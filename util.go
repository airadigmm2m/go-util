// (c) 2014 Intellimatics LLC. All Rights Reserved.
package util

import (
	"fmt"
	"os"
)

func Env(name string, def ...string) string {
	val := os.Getenv(name)
	if val != "" {
		return val
	}
	if len(def) > 0 {
		return def[0]
	}
	fmt.Printf("Missing required environmental variable: %s\n", name)
	os.Exit(1)
	return ""
}
