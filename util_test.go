// (c) 2014 Intellimatics LLC. All Rights Reserved.
package util_test

import (
	//. "bitbucket.org/airadigmm2m/sms/util"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Util library", func() {

	BeforeEach(func() {
		// Nothing to do yet
	})

	Describe("Given an util", func() {
		Context("with an no interestig input", func() {
			It("should do nothing", func() {
				Ω("a").Should(Equal("a"))
			})
		})
	})
})
