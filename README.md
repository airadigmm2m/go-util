# Go Util

Common utilities for [Go](http://golang.org) programs.

## Env

Settings are sent to deployed services using environmental variables. The Env() util provides
a simple way to retrieve an environmental variable or exit the program.